#SingleInstance, Force
#Include %A_ScriptDir%\Library\ffxiv-crafting-library.ahk

SetTitleMatchMode, 2
DetectHiddenWindows, On

IsRunning := False
IsPaused := False
IsCancelling := False
RunningProcess := 0
StatusCache := ""

; Menu Bar
Menu, HelpMenu, Add, &Shortcuts, MenuHandler
Menu, MyMenuBar, Add, &Help, :HelpMenu
Gui, Menu, MyMenuBar

; ListBox Controllers
Gui, Font, s11, Calibri
Gui, Add, Text,, Crafting Scripts
Gui, Font, s13 bold, Calibri
Gui, Add, ListBox, vMyListBox gMyListBox w480 r10

; Quantity and Synthesis Timer Controllers
Gui, Font
Gui, Font, s11, Calibri
Gui, Add, Text, vQuantityText, Quantity
Gui, Add, Edit, limit3 x+10 vQuantity w50, 1
Gui, Add, Text, x+10 w1 h25 0x7
Gui, Add, Text, x+10, Synthesis Timer
Gui, Add, Radio, x+10 Checked vNQ, NQ
Gui, Add, Radio, x+10 vHQ, HQ
Gui, Add, Radio, x+10 vCollectable, Collectable

; Script Controllers
BtnWidth := 75
BtnHeight := 25
Gui, Add, Button, x160 w%BtnWidth% h%BtnHeight% vResumeButton gResumeButton, Resume
Gui, Add, Button, x+10 w%BtnWidth% h%BtnHeight% vPauseButton gPauseButton, Pause
Gui, Add, Button, x+10 w%BtnWidth% h%BtnHeight% vCancelButton gCancelButton, Cancel
GuiControl, Hide, ResumeButton
GuiControl, Hide, PauseButton
GuiControl, Hide, CancelButton
Gui, Add, Button, x+10 w%BtnWidth% h%BtnHeight% vCraftButton gCraft, Craft

; Status GUI
Gui, Add, Text, x10 w480 h1 0x7
Gui, Add, Text, x10 w240 vStatusText, Ready

; Load Scripts and Show UI
Loop, %A_WorkingDir%\Scripts\*.* {
  FileName := StrReplace(A_LoopFileName, ".ahk", "")
  GuiControl,, MyListBox, %FileName%
}
Gui, Show,, FFXIV Crafting v4.0.0
Return


; Label Functions
^PgDn::
CancelButton:
  GuiControl, Text, StatusText, Canceling...
  IsCancelling := True
  BlockInput, MouseMoveOff
  Process, Close, %RunningProcess%
  if IsPaused {
    Pause, Off
  }
Return

PgDn::
PauseButton:
  if !IsPaused {
    IsPaused := True
    GuiControlGet, StatusText
    StatusCache := StatusText
    TogglePauseProcess(RunningProcess)
    GuiControl, Text, StatusText, Paused
    GuiControl, Hide, PauseButton
    GuiControl, Move, ResumeButton, x330
    GuiControl, Show, ResumeButton
    Pause, On
  }
Return

PgUp::
ResumeButton:
  if IsPaused {
    IsPaused := False
    GuiControl, Text, StatusText, %StatusCache%
    TogglePauseProcess(RunningProcess)
    GuiControl, Hide, ResumeButton
    GuiControl, Move, PauseButton, x330
    GuiControl, Show, PauseButton
    Pause, Off
  }
Return

MenuHandler:
  MsgBox, Craft (CTRL + B)`n`nPause (Page Down)`n`nResume (Page Up)`n`nCancel (Ctrl + Page Down)
Return

MyListBox:
  if (A_GuiEvent <> "DoubleClick")
    Return
; Otherwise, the user double-clicked a list item, so treat that the same as pressing OK.
; So fall through to the next label.

^b::
Craft:
  GuiControlGet, MyListBox
  if (!IsRunning and (MyListBox <> "")) {
    Gui, Submit, NoHide
    
    ; Refactor into ConfirmCrafting().
    IsRunning := True
    MsgBox, 4,, Crafting the Following:`n`nQuantity: %Quantity%`n`nScript: %MyListBox%
    IfMsgBox, No
    {
      IsRunning := False
      Return
    }

    EnterCraftState()
    QualityType := GetQualityTimer(NQ, HQ, Collectable)
    Loop %Quantity% {
      GuiControl, Text, StatusText, Running - Crafting (%A_Index%/%Quantity%)
      Start()
      if IsCancelling {
        Break
      }
      RunWait, %A_AhkPath% "%A_WorkingDir%\Scripts\%MyListBox%.ahk",,, RunningProcess
      if IsCancelling {
        Break
      }
      Finish(QualityType)
    }
    ResetState()

    if (ErrorLevel = "ERROR")
      MsgBox Could not launch the specified file. Perhaps it is not associated with anything.
  } else {
    MsgBox, Please select a script.
  }
Return

; Helper Functions
GetQualityTimer(NQ, HQ, Collectable) {
  if NQ {
    return "NQ"
  } else if HQ {
    return "HQ"
  } else if Collectable {
    return "CC"
  }
}

TogglePauseProcess(pid) {
  PostMessage, 0x111, 65306,,, ahk_pid %pid%
}

; GUI Helper Functions
EnterCraftState() {
  GuiControl, Move, PauseButton, x330
  GuiControl, Move, CancelButton, x415
  GuiControl, Show, PauseButton
  GuiControl, Show, CancelButton
  GuiControl, Hide, CraftButton
}

ResetState() {
  global
  IsRunning := False
  IsPaused := False
  IsCancelling := False
  RunningProcess := 0
  StatusCache := ""
  GuiControl, Hide, ResumeButton
  GuiControl, Hide, PauseButton
  GuiControl, Hide, CancelButton
  GuiControl, Show, CraftButton
  GuiControl, Text, StatusText, Ready
}

GuiClose:
GuiEscape:
ExitApp
