# Change Log

### 4.0.1
* Fixed issue with canceling a script while it is paused.
* Fixed issue with being unable to run scripts after canceling a script.

### 4.0.0
* Application overhaul -- now there is a user interface for the crafting
  - All scripts will be executed through `ffxiv-crafting-gui` now
  - Scripts no longer require `Start()` and `Finish(qualityType)`
  - Scripts all within `Scripts` will be displayed on the UI
* Framework updated to block user input during selection periods (such as confirming menus)
* Issue with automation randomly selecting a nearby object or player (the confirm action was firing too quickly)

### 3.1.0
* New Stable Release.
* Several aliases added to commonly used skills.
* Fixed pathing inconsistency in library.
* README Updated.
* Cleaned up project structure.

### 3.0.4
* Start() and Finish() functions added.
* Alias example added (`ToT()`)
* README addition

### 3.0.0
* Libary released.
