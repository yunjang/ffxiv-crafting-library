/*
    FFXIV Crafting Library [v4.0.0]
    Author: nullbahamut
*/

WinGet, pid, List, FINAL FANTASY XIV

#SingleInstance force
#Include %A_WorkingDir%\Config\skill-keybind.ahk

; Variables
hotbarCache         := 1
actionDelay         := 1650 + latencyBuffer
buffDelay           := 650 + latencyBuffer
longBuffDelay       := 2500 + latencyBuffer
modifierDelay       := 75
hotbarDelay         := 5
menuDelay           := 400 + latencyBuffer
readyItem           := 1550 + latencyBuffer
completeNQ          := 1700 + latencyBuffer
completeHQ          := 2600 + latencyBuffer
completeCC          := 3650 + latencyBuffer

; #Include %A_ScriptDir%\..\Config\script-control.ahk

/*
    *********************************************************************************************************
    Disclaimer: Below begins the functions of this library. Modify them only if you know what you're doing.
    *********************************************************************************************************
*/

; Generic Functions
PressKey(inputKey, modifier, hotbar, delay, repeat := 1) {
    global
    if (hotbar != "" and hotbarCache != hotbar) {
        hotbarCache := hotbar
        SwapHotbar(hotbar)
    }

    Loop %repeat% {
        ModifierKey(modifier, "down")
        Loop 5 {
            ControlSend,, {%inputKey%}, ahk_id %pid1%
            Sleep 100
        }
        Sleep %delay%
        ModifierKey(modifier, "up")
    }
    Return
}

SwapHotbar(hotbar) {
    global
    ControlSend,, {Shift Down}, ahk_id %pid1%
    Sleep %hotbarDelay%
    Loop 5 {
        ControlSend,, {%hotbar%}, ahk_id %pid1%
        Sleep %hotbarDelay%
    }
    Sleep %hotbarDelay%
    ControlSend,, {Shift Up}, ahk_id %pid1%
    Return
}

ModifierKey(modifierKey, direction) {
    global
    if (modifierKey != "") {
        ControlSend,, {%modifierKey% %direction%}, ahk_id %pid1%
        Sleep %modifierDelay%
    }
    Return
}

/*
    UI Navigation Functions
*/
Start() {
    global
    StartUpWait()
    FocusWindow()
    ConfirmItemSelection()
    ReadyItem()
    Return
}

StartUpWait() {
    global
    BlockInput, MouseMove
    Sleep 150
    Return
}

FocusWindow() {
    global
    ControlSend,, {%navigateRight%}, ahk_id %pid1%
    Sleep %modifierDelay%
    Return
}

ConfirmItemSelection() {
    global
    Loop 2 {
        ControlSend,, {%confirmKey%}, ahk_id %pid1%
        Sleep 150
    }
    Return
}

ReadyItem() { 
    global
    Sleep %readyItem%
    BlockInput, MouseMoveOff
    Return
}

Finish(qualityType := "nq") {
    global
    Reset()
    FinishItem(qualityType)
    Return
}

Reset() {
    global
    SwapHotbar(1)
    Return
}

FinishItem(qualityType) {
    global
    if (qualityType = "nq") {
        Sleep %completeNQ%
    } else if (qualityType = "hq") {
        Sleep %completeHQ%
    } else if (qualityType = "cc") {
        BlockInput, MouseMove
        Sleep %menuDelay%
        ControlSend,, {%confirmKey%}, ahk_id %pid1%
        Sleep %modifierDelay%
        ControlSend,, {%confirmKey%}, ahk_id %pid1%
        Sleep %modifierDelay%
        BlockInput, MouseMoveOff
        Sleep %completeCC%
    }
    Return
}



/*
 Functions per Skills (and their aliases)
*/

; Synthesis
BasicSynthesis(repeat := 1) {
    global
    PressKey(BasicSynthesis.inputKey, BasicSynthesis.modifier, BasicSynthesis.hotbar, actionDelay, repeat)
    Return
}

StandardSynthesis(repeat := 1) {
    global
    PressKey(StandardSynthesis.inputKey, StandardSynthesis.modifier, StandardSynthesis.hotbar, actionDelay, repeat)
    Return
}

FlawlessSynthesis(repeat := 1) {
    global
    PressKey(FlawlessSynthesis.inputKey, FlawlessSynthesis.modifier, FlawlessSynthesis.hotbar, actionDelay, repeat)
    Return
}

FS(repeat := 1) {
    global
    FlawlessSynthesis(repeat)
    Return
}

CarefulSynthesis(repeat := 1) {
    global
    PressKey(CarefulSynthesis.inputKey, CarefulSynthesis.modifier, CarefulSynthesis.hotbar, actionDelay, repeat)
    Return
}

CarefulSynthesis2(repeat := 1) {
    global
    PressKey(CarefulSynthesis2.inputKey, CarefulSynthesis2.modifier, CarefulSynthesis2.hotbar, actionDelay, repeat)
    Return
}

CS2(repeat := 1) {
    global
    CarefulSynthesis2(repeat)
    Return
}

CarefulSynthesis3(repeat := 1) {
    global
    PressKey(CarefulSynthesis3.inputKey, CarefulSynthesis3.modifier, CarefulSynthesis3.hotbar, actionDelay, repeat)
    Return
}

CS3(repeat := 1) {
    global
    CarefulSynthesis3(repeat)
    Return
}

PieceByPiece(repeat := 1) {
    global
    PressKey(PieceByPiece.inputKey, PieceByPiece.modifier, PieceByPiece.hotbar, actionDelay, repeat)
    Return
}

PBP(repeat := 1) {
    global
    PieceByPiece(repeat)
    Return
}

RapidSynthesis(repeat := 1) {
    global
    PressKey(RapidSynthesis.inputKey, RapidSynthesis.modifier, RapidSynthesis.hotbar, actionDelay, repeat)
    Return
}

RapidSynthesis2(repeat := 1) {
    global
    PressKey(RapidSynthesis2.inputKey, RapidSynthesis2.modifier, RapidSynthesis2.hotbar, actionDelay, repeat)
    Return
}

FocusedSynthesis(repeat := 1) {
    global
    PressKey(FocusedSynthesis.inputKey, FocusedSynthesis.modifier, FocusedSynthesis.hotbar, actionDelay, repeat)
    Return
}

MuscleMemory(repeat := 1) {
    global
    PressKey(MuscleMemory.inputKey, MuscleMemory.modifier, MuscleMemory.hotbar, actionDelay, repeat)
    Return
}

BrandOfEarth(repeat := 1) {
    global
    PressKey(BrandOfEarth.inputKey, BrandOfEarth.modifier, BrandOfEarth.hotbar, actionDelay, repeat)
    Return
}

BrandOfFire(repeat := 1) {
    global
    PressKey(BrandOfFire.inputKey, BrandOfFire.modifier, BrandOfFire.hotbar, actionDelay, repeat)
    Return
}

BrandOfIce(repeat := 1) {
    global
    PressKey(BrandOfIce.inputKey, BrandOfIce.modifier, BrandOfIce.hotbar, actionDelay, repeat)
    Return
}

BrandOfLightning(repeat := 1) {
    global
    PressKey(BrandOfLightning.inputKey, BrandOfLightning.modifier, BrandOfLightning.hotbar, actionDelay, repeat)
    Return
}

BrandOfWater(repeat := 1) {
    global
    PressKey(BrandOfWater.inputKey, BrandOfWater.modifier, BrandOfWater.hotbar, actionDelay, repeat)
    Return
}

BrandOfWind(repeat := 1) {
    global
    PressKey(BrandOfWind.inputKey, BrandOfWind.modifier, BrandOfWind.hotbar, actionDelay, repeat)
    Return
}

; Quality
BasicTouch(repeat := 1) {
    global
    PressKey(BasicTouch.inputKey, BasicTouch.modifier, BasicTouch.hotbar, actionDelay, repeat)
    Return
}

StandardTouch(repeat := 1) {
    global
    PressKey(StandardTouch.inputKey, StandardTouch.modifier, StandardTouch.hotbar, actionDelay, repeat)
    Return
}

AdvancedTouch(repeat := 1) {
    global
    PressKey(AdvancedTouch.inputKey, AdvancedTouch.modifier, AdvancedTouch.hotbar, actionDelay, repeat)
    Return
}

HastyTouch(repeat := 1) {
    global
    PressKey(HastyTouch.inputKey, HastyTouch.modifier, HastyTouch.hotbar, actionDelay, repeat)
    Return
}

HastyTouch2(repeat := 1) {
    global
    PressKey(HastyTouch2.inputKey, HastyTouch2.modifier, HastyTouch2.hotbar, actionDelay, repeat)
    Return
}

ByregotsBlessing(repeat := 1) {
    global
    PressKey(ByregotsBlessing.inputKey, ByregotsBlessing.modifier, ByregotsBlessing.hotbar, actionDelay, repeat)
    Return
}

BBlessing(repeat := 1) {
    global
    ByregotsBlessing(repeat)
    Return
}

ByregotsBrow(repeat := 1) {
    global
    PressKey(ByregotsBrow.inputKey, ByregotsBrow.modifier, ByregotsBrow.hotbar, actionDelay, repeat)
    Return
}

PreciseTouch(repeat := 1) {
    global
    PressKey(PreciseTouch.inputKey, PreciseTouch.modifier, PreciseTouch.hotbar, actionDelay, repeat)
    Return
}

FocusedTouch(repeat := 1) {
    global
    PressKey(FocusedTouch.inputKey, FocusedTouch.modifier, FocusedTouch.hotbar, actionDelay, repeat)
    Return
}

PatientTouch(repeat := 1) {
    global
    PressKey(PatientTouch.inputKey, PatientTouch.modifier, PatientTouch.hotbar, actionDelay, repeat)
    Return
}

PrudentTouch(repeat := 1) {
    global
    PressKey(PrudentTouch.inputKey, PrudentTouch.modifier, PrudentTouch.hotbar, actionDelay, repeat)
    Return
}

; CP
ComfortZone(repeat := 1) {
    global
    PressKey(ComfortZone.inputKey, ComfortZone.modifier, ComfortZone.hotbar, buffDelay, repeat)   
    Return
}

CZ(repeat := 1) {
    global
    ComfortZone(repeat)
    Return
}

Rumination(repeat := 1) {
    global
    PressKey(Rumination.inputKey, Rumination.modifier, Rumination.hotbar, buffDelay, repeat)   
    Return
}

TricksOfTheTrade(repeat := 1) {
    global
    PressKey(TricksOfTheTrade.inputKey, TricksOfTheTrade.modifier, TricksOfTheTrade.hotbar, buffDelay, repeat)   
    Return
}

ToT(repeat := 1) {
    global
    TricksOfTheTrade(repeat)
    Return
}

; Durability
MastersMend(repeat := 1) {
    global
    PressKey(MastersMend.inputKey, MastersMend.modifier, MastersMend.hotbar, longBuffDelay, repeat)   
    Return
}

Mend(repeat := 1) {
    global
    MastersMend(repeat := 1)
    Return
}

MastersMend2(repeat := 1) {
    global
    PressKey(MastersMend2.inputKey, MastersMend2.modifier, MastersMend2.hotbar, longBuffDelay, repeat)   
    Return
}

Mend2(repeat := 1) {
    global
    MastersMend2(repeat)
    Return
}

WasteNot(repeat := 1) {
    global
    PressKey(WasteNot.inputKey, WasteNot.modifier, WasteNot.hotbar, buffDelay, repeat)   
    Return
}

WasteNot2(repeat := 1) {
    global
    PressKey(WasteNot2.inputKey, WasteNot2.modifier, WasteNot2.hotbar, buffDelay, repeat)   
    Return
}

Manipulation(repeat := 1) {
    global
    PressKey(Manipulation.inputKey, Manipulation.modifier, Manipulation.hotbar, buffDelay, repeat)   
    Return
}

Manipulation2(repeat := 1) {
    global
    PressKey(Manipulation2.inputKey, Manipulation2.modifier, Manipulation2.hotbar, buffDelay, repeat)   
    Return
}

; Buffs
InnerQuiet(repeat := 1) {
    global
    PressKey(InnerQuiet.inputKey, InnerQuiet.modifier, InnerQuiet.hotbar, buffDelay, repeat)   
    Return
}

IQ(repeat := 1) {
    global
    InnerQuiet(repeat)
    Return
}

SteadyHand(repeat := 1) {
    global
    PressKey(SteadyHand.inputKey, SteadyHand.modifier, SteadyHand.hotbar, buffDelay, repeat)   
    Return
}

SH(repeat := 1) {
    global
    SteadyHand(repeat)
    Return
}

SteadyHand2(repeat := 1) {
    global
    PressKey(SteadyHand2.inputKey, SteadyHand2.modifier, SteadyHand2.hotbar, buffDelay, repeat)   
    Return
}

SH2(repeat := 1) {
    global
    SteadyHand2(repeat)
    Return
}

Ingenuity(repeat := 1) {
    global
    PressKey(Ingenuity.inputKey, Ingenuity.modifier, Ingenuity.hotbar, buffDelay, repeat)   
    Return
}

Ingenuity2(repeat := 1) {
    global
    PressKey(Ingenuity2.inputKey, Ingenuity2.modifier, Ingenuity2.hotbar, buffDelay, repeat)   
    Return
}

GreatStrides(repeat := 1) {
    global
    PressKey(GreatStrides.inputKey, GreatStrides.modifier, GreatStrides.hotbar, buffDelay, repeat)   
    Return
}

GS(repeat := 1) {
    global
    GreatStrides(repeat)
    Return
}

Innovation(repeat := 1) {
    global
    PressKey(Innovation.inputKey, Innovation.modifier, Innovation.hotbar, buffDelay, repeat)   
    Return
}

MakersMark(repeat := 1) {
    global
    PressKey(MakersMark.inputKey, MakersMark.modifier, MakersMark.hotbar, longBuffDelay, repeat)   
    Return
}

MM(repeat := 1) {
    global
    MakersMark(repeat)
    Return
}

InitialPreparations(repeat := 1) {
    global
    PressKey(InitialPreparations.inputKey, InitialPreparations.modifier, InitialPreparations.hotbar, buffDelay, repeat)   
    Return
}

NameOfEarth(repeat := 1) {
    global
    PressKey(NameOfEarth.inputKey, NameOfEarth.modifier, NameOfEarth.hotbar, buffDelay, repeat)   
    Return
}

NameOfFire(repeat := 1) {
    global
    PressKey(NameOfFire.inputKey, NameOfFire.modifier, NameOfFire.hotbar, buffDelay, repeat)   
    Return
}

NameOfIce(repeat := 1) {
    global
    PressKey(NameOfIce.inputKey, NameOfIce.modifier, NameOfIce.hotbar, buffDelay, repeat)   
    Return
}

NameOfLightning(repeat := 1) {
    global
    PressKey(NameOfLightning.inputKey, NameOfLightning.modifier, NameOfLightning.hotbar, buffDelay, repeat)   
    Return
}

NameOfWater(repeat := 1) {
    global
    PressKey(NameOfWater.inputKey, NameOfWater.modifier, NameOfWater.hotbar, buffDelay, repeat)   
    Return
}

NameOfWind(repeat := 1) {
    global
    PressKey(NameOfWind.inputKey, NameOfWind.modifier, NameOfWind.hotbar, buffDelay, repeat)   
    Return
}

; Specialist
InnovativeTouch(repeat := 1) {
    global
    PressKey(InnovativeTouch.inputKey, InnovativeTouch.modifier, InnovativeTouch.hotbar, buffDelay, repeat)   
    Return
}

ByregotsMiracle(repeat := 1) {
    global
    PressKey(ByregotsMiracle.inputKey, ByregotsMiracle.modifier, ByregotsMiracle.hotbar, buffDelay, repeat)   
    Return
}

Reinforce(repeat := 1) {
    global
    PressKey(Reinforce.inputKey, Reinforce.modifier, Reinforce.hotbar, buffDelay, repeat)   
    Return
}

Refurbish(repeat := 1) {
    global
    PressKey(Refurbish.inputKey, Refurbish.modifier, Refurbish.hotbar, buffDelay, repeat)   
    Return
}

Reflect(repeat := 1) {
    global
    PressKey(Reflect.inputKey, Reflect.modifier, Reflect.hotbar, buffDelay, repeat)   
    Return
}

; Others
Observe(repeat := 1) {
    global
    PressKey(Observe.inputKey, Observe.modifier, Observe.hotbar, actionDelay, repeat)   
    Return
}

Reclaim(repeat := 1) {
    global
    PressKey(Reclaim.inputKey, Reclaim.modifier, Reclaim.hotbar, buffDelay, repeat)   
    Return
}
