/*
    Set all the abilities based on their keybinds down below.
    You will need to set them by the following:
    - inputKey: your keypress for the skill
    - modifier: alt, ctrl, other buttons you press to hit other hotbar keys
    - hotbar: what hotbar the skill is on

    By specifying these, you will no longer have to specify specific keys for skills.
    Only caveat: this requires all keybinds to be the same across all jobs.
*/

/*
    Miscellaneous Settings
*/
latencyBuffer           := 0        ; Set this in millisecond (ms) values if your connection is slow.
confirmKey              := "V"        ; Your keybind for the Confirm action. 
navigateRight           := "Right"    ; Your keybind for navigating right on the menu (the finger cursor)

/*
    Synthesis Skills
*/
BasicSynthesis          := { inputKey: "-", modifier: "CTRL", hotbar: "" }
StandardSynthesis       := { inputKey: "=", modifier: "CTRL", hotbar: "" }
FlawlessSynthesis       := { inputKey: "3", modifier: "", hotbar: "2" }
CarefulSynthesis        := { inputKey: "1", modifier: "", hotbar: "1" }
CarefulSynthesis2       := { inputKey: "5", modifier: "CTRL", hotbar: "" }
CarefulSynthesis3       := { inputKey: "1", modifier: "", hotbar: "1" }
PieceByPiece            := { inputKey: "=", modifier: "", hotbar: "2" }
RapidSynthesis          := { inputKey: "", modifier: "", hotbar: "" }
RapidSynthesis2         := { inputKey: "4", modifier: "", hotbar: "2" }
FocusedSynthesis        := { inputKey: "8", modifier: "", hotbar: "2" }
MuscleMemory            := { inputKey: "0", modifier: "ALT", hotbar: "" }
BrandOfEarth            := { inputKey: "1", modifier: "", hotbar: "1" }
BrandOfFire             := { inputKey: "1", modifier: "", hotbar: "1" }
BrandOfIce              := { inputKey: "1", modifier: "", hotbar: "1" }
BrandOfLightning        := { inputKey: "1", modifier: "", hotbar: "1" }
BrandOfWater            := { inputKey: "1", modifier: "", hotbar: "1" }
BrandOfWind             := { inputKey: "1", modifier: "", hotbar: "1" }

/*
    Quality Skills
*/
BasicTouch              := { inputKey: "3", modifier: "", hotbar: "1" }
StandardTouch           := { inputKey: "9", modifier: "CTRL", hotbar: "" }
AdvancedTouch           := { inputKey: "0", modifier: "CTRL", hotbar: "" }
HastyTouch              := { inputKey: "2", modifier: "", hotbar: "1" }
HastyTouch2             := { inputKey: "7", modifier: "", hotbar: "1" }
ByregotsBlessing        := { inputKey: "6", modifier: "", hotbar: "1" }
ByregotsBrow            := { inputKey: "1", modifier: "", hotbar: "1" }
PreciseTouch            := { inputKey: "1", modifier: "", hotbar: "1" }
FocusedTouch            := { inputKey: "9", modifier: "", hotbar: "2" }
PatientTouch            := { inputKey: "1", modifier: "", hotbar: "1" }
PrudentTouch            := { inputKey: "5", modifier: "", hotbar: "2" }

/*
    CP Skills
*/
ComfortZone             := { inputKey: "8", modifier: "", hotbar: "1" }
Rumination              := { inputKey: "1", modifier: "", hotbar: "1" }
TricksOfTheTrade        := { inputKey: "1", modifier: "CTRL", hotbar: "" }

/*
    Durability Skills
*/
MastersMend             := { inputKey: "1", modifier: "ALT", hotbar: "" }
MastersMend2            := { inputKey: "2", modifier: "ALT", hotbar: "" }
WasteNot                := { inputKey: "=", modifier: "", hotbar: "1" }
WasteNot2               := { inputKey: "0", modifier: "", hotbar: "1" }
Manipulation            := { inputKey: "8", modifier: "ALT", hotbar: "" }
Manipulation2           := { inputKey: "9", modifier: "ALT", hotbar: "" }

/*
    Buff Skills
*/
InnerQuiet              := { inputKey: "9", modifier: "", hotbar: "1" }
SteadyHand              := { inputKey: "4", modifier: "", hotbar: "1" }
SteadyHand2             := { inputKey: "-", modifier: "", hotbar: "1" }
Ingenuity               := { inputKey: "3", modifier: "CTRL", hotbar: "" }
Ingenuity2              := { inputKey: "4", modifier: "CTRL", hotbar: "" }
GreatStrides            := { inputKey: "5", modifier: "", hotbar: "1" }
Innovation              := { inputKey: "2", modifier: "CTRL", hotbar: "" }
MakersMark              := { inputKey: "1", modifier: "", hotbar: "2" }
InitialPreparations     := { inputKey: "1", modifier: "", hotbar: "1" }
NameOfEarth             := { inputKey: "1", modifier: "", hotbar: "1" }
NameOfFire              := { inputKey: "1", modifier: "", hotbar: "1" }
NameOfIce               := { inputKey: "1", modifier: "", hotbar: "1" }
NameOfLightning         := { inputKey: "1", modifier: "", hotbar: "1" }
NameOfWater             := { inputKey: "1", modifier: "", hotbar: "1" }
NameOfWind              := { inputKey: "1", modifier: "", hotbar: "1" }

/*
    Specialist Skills
*/
InnovativeTouch         := { inputKey: "1", modifier: "", hotbar: "1" }
ByregotsMiracle         := { inputKey: "1", modifier: "", hotbar: "1" }
Reinforce               := { inputKey: "1", modifier: "", hotbar: "1" }
Refurbish               := { inputKey: "1", modifier: "", hotbar: "1" }
Reflect                 := { inputKey: "1", modifier: "", hotbar: "1" }

/*
    Other Skills
*/
Observe                 := { inputKey: "3", modifier: "", hotbar: "5" }
Reclaim                 := { inputKey: "", modifier: "", hotbar: "" }