#SingleInstance force
#Include %A_ScriptDir%\..\Library\ffxiv-crafting-library.ahk

!^c::
    InputBox, quantity, Enter Quantity, Enter how many you want to craft.
    InputBox, ingenuity, Need Ingenuity?, Need Ingenuity?
    InputBox, synth, Number of Synths?, How many synths?
    Loop %quantity% {
        Start()
        if (ingenuity = "y") {
            Ingenuity2()
        }
        CarefulSynthesis3(synth)
        Finish("NQ")
    }
Return

^`::
    BlockInput, MouseMove
    ControlSend,, {Left}, ahk_id %pid1%
    Sleep, 25
    ControlSend,, {Left}, ahk_id %pid1%
    Sleep, 25
    ControlSend,, {V}, ahk_id %pid1%
    BlockInput, MouseMoveOff
Return
