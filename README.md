# FFXIV Crafting Library [v4.0.0]
Crafting Automation made for FFXIV using AutoHotkey. This is the third rendition of this library after I finally got un-lazy to make a better one.

The library has been updated to run as a GUI Application using AutoHotKey. As a result, the usage as changed. You'll be able to find more information in this README.

[Please view the Changelog here.](https://gitlab.com/nullbahamut/ffxiv-crafting-library/blob/master/CHANGELOG.md)

### General Usage Note
Before using this, I advise that you have the same keybinds across all your crafters to get more mileage out of this. But otherwise, you don't need to as long as you're okay with changing keybinds in the configuration file.

This library utilizes the `skill-binds.ahk` to figure out the keybinds for all your crafting skills. Simply punch in your key, a modifier, and the hotbar that it's under (this is optional but recommended).

e.g.
```
BasicTouch := { inputKey: "1", modifierKey: "CTRL", hotbar: "1" }
```

inputKey: your actual key that you press for the skill.

modifierKey: if you use alternate key in conjunction of the actual key.

hotbar: the hotbar that the skill is on.

By setting up all of the input bindings, you now only need to reference the skill name in a script and it will handle the proper key presses and hotbar swapping for you without you having to explicitly state which key to press.

### How to Use
The framework now has a user interface associated with it, the steps to use it follows:

- Select a Script from the Crafting Scripts List Box
- Enter the Quantity and then the Quality Timer (NQ, HQ, Collectable) to ensure that the timing at the end is correct
- Press Craft
- Confirm from the Pop Up
- Enjoy automated crafting

The buttons themselves all have hot keys associated with them as well, here are the associated key binds:
- Craft (CTRL + B)
- Cancel (CTRL + Page Down)
- Pause (Page Down)
- Resume (Page Up)

The application loads all scripts available from the `Scripts` folder. So you'll be able to create your own scripts as well. I recommend reading the rest of the README to get a better idea or simply open existing scripts that I've included to get a better idea.

### Library Include
When writing a new script out for a craft with AutoHotkey, you'll need the following:

```
#SingleInstance, Force
#Include %A_ScriptDir%\..\Library\ffxiv-crafting-library.ahk
```

The first line will only allow a single instance of your script to exist. The second line will navigate to your current directory and into the library folder to import the actual library into your script.

### Things to Note
The library handles hotbar shifting via the `Shift` key (e.g. `Shift + 2` to move to the second hotbar). So if you have something else bound for shift, this will prevent the library from working.

Modifier keys are limited right now to `ALT` and `CTRL` keys. If you have those bound to something else, simply leave the `modifierKey` as blank (`""`) and it will hotbar shift instead to use the ability.

Lastly, I understand this README is a mess. I'll come back and tidy it up when I am feeling more Englishy. :)
